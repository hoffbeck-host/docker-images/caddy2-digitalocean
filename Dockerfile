ARG VERSION
FROM caddy:${VERSION}-builder AS builder
RUN xcaddy build --with github.com/caddy-dns/digitalocean

FROM caddy:${VERSION}
MAINTAINER Christian Hoffbeck <https://github.com/hoffbeck>
COPY --from=builder /usr/bin/caddy /usr/bin/caddy