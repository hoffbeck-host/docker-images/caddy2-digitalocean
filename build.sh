#! /bin/sh

apk add curl jq

DOCKERHUB_ORG_NAME="zonecreator"
BUILD_NAME="caddy2-digitalocean"
CADDY_LATEST_BUILDER_TAG=$(curl -sL "https://registry.hub.docker.com/v2/repositories/library/caddy/tags/?page_size=200" | jq -r ".results[].name" | grep -E "^2\.[0-9]+\.[0-9]+\-builder$" | head -n1)
DO_BUILD_TAG=$(curl -sL "https://registry.hub.docker.com/v2/repositories/zonecreator/caddy2-digitalocean/tags/?page_size=200" | jq -r ".results[].name" | grep -E "^2\.[0-9]+\.[0-9]+\-builder$" | head -n1)
CADDY_TAG=$(echo "${CADDY_LATEST_BUILDER_TAG}" | awk -F"-" '{print $1}')
DO_CADDY_TAG=$(echo "${DO_BUILD_TAG}" | awk -F"-" '{print $1}')

echo "${CADDY_TAG}"
echo "${DO_CADDY_TAG}"
if [ "${CADDY_TAG}" == "${DO_CADDY_TAG}" ]; then
	echo "Latest tag is the same as current tag."
	exit 2
fi

DOCKER_BUILD_TAG_VERSION="${DOCKERHUB_ORG_NAME}/${BUILD_NAME}:${CADDY_TAG}"
DOCKER_BUILD_TAG_LATEST="${DOCKERHUB_ORG_NAME}/${BUILD_NAME}:latest"

docker buildx build --platform=linux/amd64 --build-arg VERSION=${CADDY_TAG} --pull --tag ${DOCKER_BUILD_TAG_VERSION} --tag ${DOCKER_BUILD_TAG_LATEST} .
docker buildx build --platform=linux/arm64 --build-arg VERSION=${CADDY_TAG} --pull --tag ${DOCKER_BUILD_TAG_VERSION} --tag ${DOCKER_BUILD_TAG_LATEST} .

docker push "${DOCKER_BUILD_TAG_VERSION}"
docker push "${DOCKER_BUILD_TAG_LATEST}"


